package com.restoApp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Transactions")

public class TransactionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @JoinColumn(name = "menuId", referencedColumnName = "id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne( cascade = CascadeType.DETACH)
    private MenuModel menuId;

    @JoinColumn(name = "userId", referencedColumnName = "id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne( cascade = CascadeType.DETACH)
    private UserModel userId;

    @CreationTimestamp
    @Column(name = "dateOfTransaction", nullable = false)
    private Date dateOfTransaction;

    @Column
    private int totalMenus;

    public TransactionModel() {
    }

    public TransactionModel(long id, MenuModel menuId, UserModel userId, Date dateOfTransaction, int totalMenus) {
        this.id = id;
        this.menuId = menuId;
        this.userId = userId;
        this.dateOfTransaction = dateOfTransaction;
        this.totalMenus = totalMenus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MenuModel getMenuId() {
        return menuId;
    }

    public void setMenuId(MenuModel menuId) {
        this.menuId = menuId;
    }

    public UserModel getUserId() {
        return userId;
    }

    public void setUserId(UserModel userId) {
        this.userId = userId;
    }

    public Date getDateOfTransaction() {
        return dateOfTransaction;
    }

    public void setDateOfTransaction(Date dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    public int getTotalMenus() {
        return totalMenus;
    }

    public void setTotalMenus(int totalMenus) {
        this.totalMenus = totalMenus;
    }
}