package com.restoApp.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
public class CategoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @OneToMany(mappedBy = "category")
    private Set<MenuModel> menu;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MenuModel> getMenu() {
        return menu;
    }

    public void setMenu(Set<MenuModel> menu) {
        this.menu = menu;
    }
}
