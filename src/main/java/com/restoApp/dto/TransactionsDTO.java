package com.restoApp.dto;

import java.io.Serializable;

public  class TransactionsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private long menuId;

    private int totalMenus;

    public TransactionsDTO() {

    }

    public TransactionsDTO(long menuId, int totalMenus) {
        this.menuId = menuId;
        this.totalMenus = totalMenus;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public int getTotalMenus() {
        return totalMenus;
    }

    public void setTotalMenus(int totalMenus) {
        this.totalMenus = totalMenus;
    }
}
