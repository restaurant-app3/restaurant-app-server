package com.restoApp.controller;

import com.restoApp.model.MenuModel;
import com.restoApp.model.UserModel;
import com.restoApp.model.TransactionModel;
import com.restoApp.dto.TransactionsDTO;
import com.restoApp.repository.MenuRepository;
import com.restoApp.repository.TransactionRepository;
import com.restoApp.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping("/api/menus")
@CrossOrigin(origins = "http://localhost:3000")

public class TransactiansController {

    public static final Logger logger = LoggerFactory.getLogger(TransactiansController.class);

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UserRepository userRepository;


    @RequestMapping(value = "/{userId}/transactions", method = RequestMethod.POST)
    public ResponseEntity<?> addTransaction(@PathVariable("userId") int userId, @RequestBody List<TransactionsDTO> transactionsDTO) {

        UserModel user = userRepository.findById(userId);

        for (TransactionsDTO loopMenu : transactionsDTO) {

            MenuModel dataMenu = menuRepository.findById(loopMenu.getMenuId());

            TransactionModel savedData = new TransactionModel();
            savedData.setMenuId(dataMenu);
            savedData.setUserId(user);
            savedData.setTotalMenus(loopMenu.getTotalMenus());

            transactionRepository.save(savedData);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    // -------------------Retrieve All Books--------------------------------------------

    @RequestMapping(value = "/transactions/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<TransactionModel>> listAllTransactions() throws SQLException, ClassNotFoundException {

        List<TransactionModel> transactions = transactionRepository.findAll();

        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }
}
