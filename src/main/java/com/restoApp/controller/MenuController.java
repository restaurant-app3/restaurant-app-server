package com.restoApp.controller;

import com.restoApp.model.MenuModel;
import com.restoApp.repository.MenuRepository;
import com.restoApp.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/menus")
@CrossOrigin(origins = "http://localhost:3000")

public class MenuController {

    public static final Logger logger = LoggerFactory.getLogger(MenuController.class);

    @Autowired
    private MenuRepository menuRepository;


    // -------------------Create a Menu-------------------------------------------

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody MenuModel menu) throws SQLException, ClassNotFoundException, IOException {

        logger.info("Creating Menu : {}", menu);

        menuRepository.save(menu);

        return new ResponseEntity<>(menu, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Menus--------------------------------------------

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<MenuModel>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<MenuModel> menus = menuRepository.findAll();

        return new ResponseEntity<>(menus, HttpStatus.OK);
    }


    // -------------------Retrieve Single Menu By Id------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching Menu with id {}", id);

        Optional<MenuModel> menu = Optional.ofNullable(menuRepository.findById(id));

        if (menu == null) {
            logger.error("Menu with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Menu with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(menu, HttpStatus.OK);
    }


    // ------------------- Update a Menu ------------------------------------------------

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody MenuModel menu) throws SQLException, ClassNotFoundException {

        logger.info("Updating Menu with id {}", id);

        MenuModel currentProduct = menuRepository.findById(id);

        currentProduct.setName(menu.getName());
        currentProduct.setImage(menu.getImage());
        currentProduct.setDescription(menu.getDescription());
        currentProduct.setPrice(menu.getPrice());
        currentProduct.setDiscount(menu.getDiscount());

        menuRepository.save(currentProduct);

        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }


    // ------------------- Delete a Menu-----------------------------------------

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching & Deleting Menu with id {}", id);

        menuRepository.deleteById(id);

        return new ResponseEntity<MenuModel>(HttpStatus.NO_CONTENT);
    }

    // ------------------- Search a Menu-----------------------------------------
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<?> search(@RequestParam("name") String name) throws SQLException, ClassNotFoundException {

        List<MenuModel> menuSearch = menuRepository.searchMenu(name);
        return new ResponseEntity<>(menuSearch, HttpStatus.OK);

    }
}