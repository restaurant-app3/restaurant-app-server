package com.restoApp.repository;

import com.restoApp.model.CategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryModel, Long> {
    CategoryModel findById(long id);
    CategoryModel findCategoryModelById(long id);

}
