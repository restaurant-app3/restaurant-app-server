package com.restoApp.repository;

import com.restoApp.model.MenuModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface MenuRepository extends JpaRepository<MenuModel, Long> {

    @Query("SELECT p FROM MenuModel p WHERE " +
            "p.name LIKE CONCAT('%',:query, '%')" )
    List<MenuModel> searchMenu(String query);

    MenuModel findById(long id);
}
