package com.restoApp.service;

import com.restoApp.model.UserModel;
import com.restoApp.dto.UserDTO;
import com.restoApp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;


	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserModel user = userRepository.findByEmail(email);

		List<SimpleGrantedAuthority> roles = null;
		if(user.getRole().equals("admin"))
		{
			roles= Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
			return new User(email,user.getPassword() ,roles);
		}
		if(user.getRole().equals("user"))
		{
			roles=Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
			return new User(email,user.getPassword(),roles);
		}
		return new User(user.getEmail(), user.getPassword(),
				new ArrayList<>());
	}

	public UserModel save(UserDTO user) {
		UserModel newUser = new UserModel();
		newUser.setEmail(user.getEmail());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setRole(user.getRole());
		return userRepository.save(newUser);
	}
}